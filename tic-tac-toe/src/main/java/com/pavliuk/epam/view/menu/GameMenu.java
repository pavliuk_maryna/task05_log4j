package com.pavliuk.epam.view.menu;

import com.pavliuk.epam.controller.GameService;
import com.pavliuk.epam.controller.RandomSymbolService;
import com.pavliuk.epam.exception.WrongTurnInputException;
import com.pavliuk.epam.model.Player;
import java.util.Scanner;
import org.apache.log4j.Logger;

public class GameMenu {

  final static Logger log = Logger.getLogger(GameMenu.class);
  private GameService gameService;
  private RandomSymbolService randomSymbolService;
  private Scanner scanner;
  private Player player2;
  private Player player1;

  public GameMenu() {
    this.gameService = new GameService();
    this.scanner = new Scanner(System.in);
    this.randomSymbolService = new RandomSymbolService();
    this.player1 = new Player();
    this.player2 = new Player();
  }

  public void run() {
    while (true) {
      log.info("Hello! This is Tic Tac Toe, the best game in the world! Do you want to play? Y/N");
      if (scanner.next().equals("Y")) {
        while (true) {
          log.info("\nEnter player 1 name");
          player1.setName(scanner.next());
          player1.setSymbol(randomSymbolService.getSymbol());
          printPlayerInfo(player1);
          log.info("\nEnter player 2 name");
          player2.setName(scanner.next());
          while (true) {
            String symbol = randomSymbolService.getSymbol();
            if (!player1.getSymbol().equals(symbol)) {
              player2.setSymbol(symbol);
              break;
            }
          }
          printPlayerInfo(player2);
          log.info("\nLets start the game");
          boolean finished = false;
          while (!finished) {
            log.info("\n"+player1.getName()
                + " it's your turn, please write: horizontal+vertical choice, example: 0+2");
            gameService.printGameField();
            makeTurn(player1.getSymbol());
            finished = isFinished(gameService);
            if (finished) {
              break;
            }
            log.info("\n"+player2.getName()
                + " it's your turn, please write: horizontal+vertical choice, example: 0+2");
            gameService.printGameField();
            makeTurn(player2.getSymbol());
            finished = isFinished(gameService);
          }
          log.info("\nRestart? Y/N");
          if (scanner.next().equals("Y")) {
            gameService.fillGameField();
          } else {
            break;
          }
        }
      } else {
        log.info("\nBye! Have a beautiful day!");
        break;
      }
    }
  }

  private boolean isFinished(GameService gameService) {
    if (gameService.win(player1.getSymbol())) {
      gameService.printGameField();
      log.info("\n"+player1.getName() + " you won!");
      return true;
    } else if (gameService.win(player2.getSymbol())) {
      gameService.printGameField();
      log.info("\n"+player2.getName() + " you won!");
      return true;
    } else if (gameService.isDraw(player1.getSymbol(), player2.getSymbol())) {
      gameService.printGameField();
      log.info("\nDraw!");
      return true;
    }
    return false;
  }

  private void makeTurn(String symbol) {
    while (true) {
      try {
        String choice = scanner.next();
        checkTurnInput(choice);
        String[] split = choice.split("\\+");
        int horizontalCoordinate = Integer.parseInt(split[0]);
        int verticalCoordinate = Integer.parseInt(split[1]);
        if (!gameService.isCoordinateEmpty(horizontalCoordinate, verticalCoordinate)) {
          log.info("\nThere is already a symbol present in this cell, please try again!");
        } else {
          gameService.putSymbol(symbol, horizontalCoordinate, verticalCoordinate);
          break;
        }
      } catch (WrongTurnInputException e) {
        log.info("\nYour specified input is wrong, try again, example: 0+2");
      }
    }
  }

  private void checkTurnInput(String turn) {
    if (!turn.matches("[0-2]\\+[0-2]")) {
      throw new WrongTurnInputException("Input does not match [0-2]+\\+[0-2] regex");
    }
  }

  private void printPlayerInfo(Player player) {
    log.info("\nPlayer name: " + player.getName() + " and symbol: " + player.getSymbol());
  }

}
