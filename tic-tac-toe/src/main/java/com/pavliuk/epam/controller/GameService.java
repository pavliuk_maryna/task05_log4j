package com.pavliuk.epam.controller;

import org.apache.log4j.Logger;

public class GameService {

  final static Logger log = Logger.getLogger(GameService.class);
  private String[][] gameField = new String[3][3];

  public GameService() {
    fillGameField();
  }

  public void printGameField() {
    System.out.print("   ");
    for (int i = 0; i < gameField.length; i++) {
      System.out.print(" " + i + " ");
    }
    System.out.print("\n");
    for (int i = 0; i < gameField.length; i++) {
      System.out.print(" " + (i) + " ");
      for (int j = 0; j < gameField[0].length; j++) {
        System.out.print(gameField[i][j]);
      }
      System.out.print("\n");
    }
  }

  public boolean isCoordinateEmpty(int horizontalCoordinate, int verticalCoordinate) {
    return gameField[horizontalCoordinate][verticalCoordinate].contains("-");
  }

  public void putSymbol(String symbol, int horizontalCoordinate, int verticalCoordinate) {
    gameField[horizontalCoordinate][verticalCoordinate] = gameField[horizontalCoordinate][verticalCoordinate]
        .replace("-", symbol);
  }

  public boolean win(String symbol) {
    // horizontal check
    for (int i = 0; i < gameField.length; i++) {
      if (gameField[i][0].contains(symbol) && gameField[i][1].contains(symbol) && gameField[i][2]
          .contains(symbol)) {
        return true;
      } else if
        //vertical check
      (gameField[0][i].contains(symbol) && gameField[1][i].contains(symbol) && gameField[2][i]
              .contains(symbol)) {
        return true;
      }
    }
    // cross check
    if (gameField[1][1].contains(symbol)) {
      return (gameField[0][0].contains(symbol) && gameField[2][2].contains(symbol)) || ((
          gameField[0][2].contains(symbol) && gameField[2][0].contains(symbol)));
    }
    return false;
  }

  public boolean isDraw(String symbol1, String symbol2) {
    for (int i = 0; i < gameField.length; i++) {
      for (int j = 0; j < gameField[0].length; j++) {
        if (!gameField[i][j].contains(symbol1) && !gameField[i][j].contains(symbol2)) {
          return false;
        }
      }
    }
    return true;
  }

  public void fillGameField() {
    for (int i = 0; i < gameField.length; i++) {
      for (int j = 0; j < gameField[0].length; j++) {
        gameField[i][j] = " - ";
      }
    }
  }
}
