package com.pavliuk.epam.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;

public class RandomSymbolService {

  private Random random;
  private List<String> symbols;
  final static Logger log = Logger.getLogger(RandomSymbolService.class);


  public RandomSymbolService() {
    this.random = new Random();
    this.symbols = new ArrayList<String>();
    symbols.add("@");
    symbols.add("$");
    symbols.add("^");
    symbols.add("x");
    symbols.add("0");
    symbols.add("%");
    symbols.add("&");
    symbols.add("#");
  }

  public String getSymbol() {
    return symbols.get(random.nextInt(symbols.size()));
  }

}
